import {Engine} from "./mud_engine/engine";
import {SocketServer} from "./mud_engine/servers/socket"

class MyEngine extends Engine {}
new MyEngine()
    .add_server(new SocketServer())
    .start();