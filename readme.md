[First](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v0.01 "Version 0.01 - A basic TCP server and stub classes for abstraction ")
&bull; [Prev](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v0.05 "Version 0.05 - Building our Engine")
&bull; [Next](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v1.01 "Version 1.01 - Managing Player Identity ")
&bull; [Latest](https://gitlab.com/benjamin.small83/mud-engine-typescript/)
# Version 1: Creating our Server and Engine

## Getting started

Per the configs in this repo, we're running at least Node 12.18.3 and we're building our typescript to match the specifications [here](https://stackoverflow.com/questions/59787574/typescript-tsconfig-settings-for-node-js-12/59787575#59787575). Our code is written to match the es2019 standard and features.

Once you check out, you'll need to install all the dependencies.

`npm install`

## Idea Behind this Version

We know our MUD engine is going to need at least two components to start with. Firstly, a server that can handle a networking connections and turn those into messages to an Engine. Secondly an Engine to handle those messages and give appropriate responses back to the server to give to our connected player.

Key requirements for finishing this version of development:

* Have a server to accept incoming TCP connections
* Have a connecting handler that accepts input and tracks a buffer from a connections
* Have a mechanism for shuttling this data into a queue for the Engine
* Have a simple engine that will read data off of the input queue and process it
* Have a simple response from the engine that echoes the input back to the connection through an output queue

We want to decouple the engine from the server because our design should allow for several different types of servers that may even be running on different hosts. The input queue and output queues can be in memory or they can be queues like rabbitmq or pubsub. At this point in development we'll implement everything in a single process. For now our server will be a simple socket server, using TCP, that you can telnet to our use a standard MUD client with. Later we'll be able to drop in a full fledged telnet server and webserver.

![Data Flow](images/data_flow_for_engines_and_servers.png)

## Subversion Progression

* [Version 0.01](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tags/v0.01 "Version 0.01 - A basic TCP server and stub classes for abstraction") - A basic TCP server and stub classes for abstraction
* [Version 0.02](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tags/v0.02 "Version 0.02 - Integrating our Server Code Into our Abstraction") - Integrating our Server Code Into our Abstraction
* [Version 0.03](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v0.03 "Version 0.03 - Line Buffering our SocketConnections") - Line Buffering our SocketConnections
* [Version 0.04](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v0.04 "Version 0.04 - Restructuring our Module") - Restructuring our Module
* [Version 0.05](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v0.05 "Version 0.05 - Firing Up an Engine") - Firing Up an Engine 
