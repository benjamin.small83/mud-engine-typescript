# IGNORE THIS FILE, USED FOR NOTES ON README

[First](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v0.01 "Version 0.01 - A basic TCP server and stub classes for abstraction ")
&bull; [Prev](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v1 "Version 1 - Creating our Server and Engine")
&bull; [Next](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tree/v1.01 "Version 1.01 - Restructuring our Module")
&bull; [Latest](https://gitlab.com/benjamin.small83/mud-engine-typescript/)


[[_TOC_]]

# Version 2: Player Identity & Handling Commands 

## Getting started

Per the configs in this repo, we're running at least Node 12.18.3 and we're building our typescript to match the specifications [here](https://stackoverflow.com/questions/59787574/typescript-tsconfig-settings-for-node-js-12/59787575#59787575). Our code is written to match the es2019 standard and features.

Once you check out, you'll need to install all the dependencies.

`npm install`

## Idea Behind this Version

<blurb>

Key requirements for finishing this version of development:

* <req>

We want to decouple the engine from the server because our design should allow for several different types of servers that may even be running on different hosts. The input queue and output queues can be in memory or they can be queues like rabbitmq or pubsub. At this point in development we'll implement everything in a single process. For now our server will be a simple socket server, using TCP, that you can telnet to our use a standard MUD client with. Later we'll be able to drop in a full fledged telnet server and webserver. 

## Subversion Progression

* [Version 1.01](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tags/v1.01 "Version 1.01 - Managing Player Identity") - Managing Player Identity 
* [Version 1.02](https://gitlab.com/benjamin.small83/mud-engine-typescript/-/tags/v1.01 "Version 1.02 - Command Lookups & Execution") - Command Lookups & Execution 
