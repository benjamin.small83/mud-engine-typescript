import {MUDObject} from "./base";
import {Input, Output, IOType, Server} from "./servers/base";
import {MUDException} from "./exceptions";

export enum EngineState {
    INIT,
    STARTED,
    SHUTDOWN
}

/**
 * The base Engine that all engines should inherit from.
 */
export abstract class Engine extends MUDObject {

    /**
     * Engines need to store a dictionary of servers for lookup
     */
    public servers: {[server_id: string]: Server} = {};

    /**
     * The amount of time to delay on {@link Engine.pulse} iterations. Can be overridden on subclasses
     * @protected
     */
    protected pulse_delay = 0;

    /**
     * The amount of time to delay on {@link Engine.tick} iterations. Can be overridden on subclasses
     * @protected
     */
    protected tick_delay = 1000;

    /**
     * The state of the Engine
     */
    public state = EngineState.INIT;

    /**
     * The method called in the {@link Engine.pulse} method while iterating {@link Input}
     * @param input Input from the {@link Engine.input_queue}
     * @protected
     */
    protected handle_input(input: Input): void {

        // Is this a new connection?
        if(input.type == IOType.CONNECT){
            console.debug("Engine.handle_input: Incoming Connection with Input", input);
            return;
        }

        // Is this a disconnect?
        if(input.type == IOType.DISCONNECT){
            console.debug("Engine.handle_input: Connection is disconnecting with Input", input);
            return;
        }

        // Is this output?
        if(input.type == IOType.OUTPUT){
            console.warn("Engine.handle_input: WARN: SOMETHING IS WRONG, GOT OUTPUT IN AN InputQueue", Input);
            return;
        }

        // This is input
        console.debug("Engine.handle_input: Got new input from Connection", input);

        let output_server = this.servers[input.server_id];
        if(output_server == undefined) {
            console.warn("COULDNT FIND OUTPUT SERVER FOR ECHO", input);
            return;
        }

        let output;

        if(input.buffer == "close")
            output = new Output(input.server_id, input.connection_id, "Good Bye!", IOType.DISCONNECT);
        else
            output = new Output(input.server_id, input.connection_id, input.buffer);
        output_server.output_queue.push(output);
    }

    /**
     * Add a server to the Engine. Will start the server if the engine is running.
     * @param server The server to add to the Engine
     * @returns Engine The instance of the engine add_server was called on, convenient for chaining
     */
    public add_server(server: Server): Engine {

        if(server.server_id in this.servers)
            throw new MUDException(`Already have ${server.server_id} in the server list!`)

        this.servers[server.server_id] = server;
        if(this.state == EngineState.STARTED)
            server.start()

        return this;
    }

    /**
     * An Engine pulse is the highest fidelity loop the Engine runs. In this loop you should handle things like IO and
     * other high resolution Events that must be calculated or handled quickly.
     */
    protected pulse(): void {
        //console.debug("pulse")

        // Loop and get all the Input to process
        for(let server of Object.values(this.servers)){
            let input = server.input_queue.shift();
            while(input != undefined){
                this.handle_input(input);
                input = server.input_queue.shift();
            }
        }
    };

    /**
     * A loop to handle game events. This is a much lower resolution loop that should be used to handle things like
     * player events and actions.
     */
    protected tick(): void {
        //console.debug("Tick")
        return
    }

    /**
     * Start up the Engine. This method is also responsible for starting up the Servers and starting the Engine.pulse
     * and {@link Engine.tick} loops.
     */
    public start() {

        this.state = EngineState.STARTED;

        // Fire up our servers
        for(let s of Object.values(this.servers))
            s.start();

        // Kick off our pulse loop
        let pulse_wrap = () => { setTimeout(pulse_wrap, this.pulse_delay); this.pulse()};
        pulse_wrap();

        // Kick off our tick loop
        let tick_wrap = () => { setTimeout(tick_wrap, this.tick_delay); this.tick()};
        tick_wrap();
    }
}
