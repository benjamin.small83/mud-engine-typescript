export class MUDObject extends Object {}

/**
 * A base Queue class that all of the queue's we'll build must subclass off of
 */
export abstract class MUDQueue extends MUDObject {

    /**
     * A queue may need an internal queue for managing data in memory.
     */
    protected abstract queue?: any;

    /**
     * All queues must support pushing data onto the top of the queue.
     * For convenience we'll return the object that's pushed on to the queue.
     */
    public abstract push(item: MUDObject): MUDObject;

    /**
     * All queues must support shifting the first object off of the queue.
     * @returns MUDObject | undefined The result will be undefined if the queue is empty
     */
    public abstract shift(): MUDObject | undefined;

    // /**
    //  * Queues may support popping the last object off of the queue
    //  */
    // abstract pop?(): MUDObject;
}