import {Connection, Input, IOType, Server, Output} from "./base";
import * as net from "net";

/**
 * A quick interface for the net.Socket.write Callback
 * @private
 */
interface _SocketWriteCB {
    (err?: Error): void;
}

export class SocketConnection extends Connection {

    /**
     * The server_id associated with this Connection
     */
    public server_id: string;

    /**
     * A unique identifier for the Connection. In this implementation we join the host and port with a colon
     */
    public connection_id: string;

    /**
     * The remote address tied to this connection
     */
    public remote_address: string | undefined;

    /**
     * The remote port tied to this connection
     */
    public remote_port: number | undefined;

    /**
     * An internal buffer for tracking input from the connection to be used for line buffering
     * @protected
     */
    protected _input_buffer: string = "";

    /**
     * A hidden variable that's used by the SocketConnection for access to the {@link SocketServer}.
     * @internal
     * */
    protected _socket_server: SocketServer;

    /**
     * A hidden variable that's used by the SocketConnection for access to the net.Socket
     * @internal
     * */
    protected _socket: net.Socket;

    /**
     * A flag to track whether the connection has been disconnected. Needed because of asynchronous methods
     * of disconnectin the socket and sending final buffers.
     */
    public disconnected = false;

    constructor(socket: net.Socket, socket_server: SocketServer) {
        super();

        this.server_id = socket_server.server_id;
        this.connection_id = `${socket.remoteAddress}:${socket.remotePort}`
        this._socket_server = socket_server;
        this._socket = socket;
        this.remote_port = socket.remotePort;
        this.remote_address = socket.remoteAddress;

        this._socket.on("data", (input) => this.input_callback(input));
        this._socket.on('close', (had_error) => this.close_callback(had_error));
        this._socket.on("error", (err) => this.error_callback(err));
    }

    /**
     * A callback registered with the socket to handle errors with the connection. In this routine
     * we need to signal {@link IOType.DISCONNECT} to the Engine so it can clean up any existing Player data
     * or references. the Engine will be responsible for pushing back an empty {@link IOType.DISCONNECT} output
     * to the {@link Server} for it to clean up the {@link Connection}.
     * @param error The error that was thrown in the connection
     */
    public error_callback(error: Error) {
        console.error("Connection Error -- ", this, error);
        // Signal to the Engine that we have a disconnected client. After the
        let input = new Input(this.server_id, this.connection_id, "", IOType.DISCONNECT);
        this._socket_server.input_queue.push(input);
    }

    /**
     * A callback registered with the socket to handling incoming data over the connection
     * @param input The input the client has sent to the server (not line buffered by default, calls whenever data comes off the socket)
     */
    public input_callback(input: Buffer | string): void {

        // Ensure input is a string primitive
        input = `${input}`;

        // Break up our inputs by line breaks
        let inputs = input.split(/\r\n|[\r\n]/);

        // If we only have one part, we didn't receive a newline. Add to buffer and return
        if(inputs.length <= 1) {
            this._input_buffer += input;
            return
        }

        for(let i = 0; i < inputs.length; i++) {

            this._input_buffer += inputs[i];

            // We want to accept all inputs except for the last input chunk, that won't be a complete message
            // due to the "input.split" creating an list element for the newline. Eg:
            // "this is a new input\nand so this this one\n".split(/\r\n|[\r\n]/)
            // (3)["this is a new input", "and so this this one", ""]
            // Also skip empty buffers (just a newline with no data)
            if(i != (inputs.length - 1) && this._input_buffer != "") {

                this._input_buffer = this._input_buffer.trim();

                console.debug(`InputQueued: ${this.remote_address}:${this.remote_port} ${this._input_buffer}`);

                // Queue the Input for the Engine
                let input = new Input(this.server_id, this.connection_id, this._input_buffer);
                this._socket_server.input_queue.push(input);
                this._input_buffer = "";
            }
        }
    }

    /**
     * Send data over the connection to the client
     * @param buff The string to send to the client
     * @param callback In the case of a slow write, an optional callback to be passed once the buffer is flushed (useful for disconnect)
     */
    public write(buff: string, callback: _SocketWriteCB | undefined = undefined){
        if(callback != undefined)
            this._socket.write(buff, "utf8", callback);
        else
            this._socket.write(buff);
    }

    /**
     * A callback registered with the socket for handling when a connection is closed
     * @param had_error Whether the connection errored on exit
     */
    public close_callback(had_error: boolean): void {
        this.disconnect();
    }

    /**
     * A method to close the connection, remove the connection from the parent SocketServer
     * and ensure the socket is destroyed.
     */
    public disconnect(): void {

        // Sometimes the chain events will call this multiple times - if we're already disconnected, don't call
        // this again
        if(this.disconnected)
            return;
        this.disconnected = true;

        /**
         * NOTE: This looks scary because we have references passed to the net.Socket library
         * I tested extensively and confirmed we were getting garbage collected
         * mud-engine v0.02
         * node v12.18.3
         * net v1.0.2
         * @benjamin.small83 12/26/2020
         */
        // Tell the server to remove references to this SocketConnection
        this._socket_server.remove_connection(this.connection_id);
        this._socket.end();
        console.info(`Connection Closed: ${this.remote_address}:${this.remote_port}`);
    }
}

/**
 * A simple SocketServer for handling incoming TCP connections
 */
export class SocketServer extends Server {

    /**
     * The hostname/address to listen for new connections on
     */
    public host = "127.0.0.1";

    /**
     * The port to listen on
     */
    public port = 5000;

    /**
     * A dictionary of open {@link SocketConnection}s that the server is handling
     */
    public connections: {[connection_id: string]: SocketConnection} = {};

    /**
     * The unique identifier for this server instance. Will be the hostname joined with the port by a comma
     */
    public server_id: string;

    /** @internal */
    protected _server: net.Server = net.createServer();

    /**
     * Can instantiate the SocketServer with params to override the default hostname and port to listen on
     * @param host The hostname/address to listen for new connections on
     * @param port The port to listen on
     */
    constructor(host?:string, port?:number) {
        super();
        this.host = host || this.host;
        this.port = port || this.port;
        this.server_id = `${this.host}:${this.port}`;
    }

    /**
     * A callback registered with the net.Server to handling incoming connections. This will register
     * the {@link SocketConnection} created in the method in the {@link SocketServer.connections} dictionary.
     * @param socket The socket making the incoming connection
     */
    public connect_callback(socket: net.Socket): void {
        console.info('New Connection - ' + socket.remoteAddress + ':' + socket.remotePort);
        let socket_connection = new SocketConnection(socket, this);
        if(socket_connection.connection_id in this.connections) {
            console.warn(`WARN: SocketConnection COLLISION ${socket_connection.connection_id}, DISCONNECTING`);
            this.connections[socket_connection.connection_id].disconnect();
        }

        this.connections[socket_connection.connection_id] = socket_connection;

        // Signal to the Engine that we have a new connection
        let input = new Input(this.server_id, socket_connection.connection_id, "", IOType.CONNECT);
        this.input_queue.push(input);
    }

    /**
     * A callback registered with the net.Server to handle the Server starting up and listening
     */
    public listen_callback(): void {
        console.info(`TCP SocketServer up and running on ${this.host}:${this.port}`)
    }

    /**
     * A method to be called by the output handling loop. The {@link Engine} will push Output onto our
     * {@Server.output_queue}
     */
    protected handle_output(output: Output){
        let connection = this.connections[output.connection_id];
        if(connection == undefined) {
            console.warn("WARN: Got Output that doesn't match a connection", output, this);
            return;
        }

        if(output.type == IOType.DISCONNECT) {
            if (output.buffer) {
                connection.write(output.buffer);
                // This is a complete hack. The callback to net.Socket.write isn't actually happening
                // after the buffer flush... so we dont' get our goodbye message sent.
                setTimeout(() => connection.disconnect(), 1);
            } else {
                connection.disconnect()
            }
        } else {
            connection.write(output.buffer);
        }
    }


    /**
     * The start method will begin listening for incoming connections and registers the appropriate handlers for them.
     */
    public start(): void {
        this._server.listen(this.port, this.host, () => this.listen_callback());
        this._server.on('connection', (socket) => this.connect_callback(socket));
        let out_wrap = () => {
            setTimeout(out_wrap, 0);
            let output = this.output_queue.shift();
            while(output != undefined){
                this.handle_output(output);
                output = this.output_queue.shift();
            }
        };
        out_wrap();
    }
}