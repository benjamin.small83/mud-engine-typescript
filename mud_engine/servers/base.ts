import {MUDObject, MUDQueue} from "../base";

/**
 * An enum to Markup IO messages with the type of IO it is. This helps us synchronize the Servers and Engines.
 * For example if a connection dies, we'll send an Input with type IOType.DISCONNECT to the Engine to disconnect
 * the player.
 */
export const enum IOType {
    INPUT,
    OUTPUT,
    CONNECT,
    DISCONNECT
}

/**
 * A class to represent Input from the {@link Connection}. The {@link Connection} will put this in the {@link Server}'s
 * {@link Server.input_queue} for the {@link Engine} to process
 */
export class Input extends MUDObject {

    /**
     * The {@link Server.server_id} this Input came from
     */
    public server_id: string;

    /**
     * The {@link Connection.connection_id} this Input came from
     */
    public connection_id: string;

    /**
     * The buffer sent from the {@link Connection}
     */
    public buffer: string;

    /**
     * The type of the input this Input is. Can be one of the {@link IOType}s
     * @default IOType.INPUT
     */
    public type: IOType;

    constructor(server_id: string, connection_id: string, buffer: string, type: IOType = IOType.INPUT) {
        super();
        this.server_id = server_id;
        this.connection_id = connection_id;
        this.buffer = buffer;
        this.type = type;
    }
}

/**
 * An InputQueue class to be registered with our {@link Engine}. This will act as a channel for a {@link Server} to send
 * it's {@link Input} through for processing by the {@link Engine}. The {@link Server} will push {@link Input} on to the
 * queue and the {@link Engine} will shift it out.
 */
export class InputQueue extends MUDQueue {

    /**
     * An internal array of {@link Input}s from the {@link Server}s to be buffered for the {@link Engine} to process.
     * @protected
     */
    protected queue: Array<Input>;

    constructor() {
        super();
        this.queue = new Array<Input>();
    }

    /**
     * Push input on to the queue
     * @param input The input to send to the Engine
     */
    public push(input: Input): Input {
        this.queue.push(input);
        return input;
    }

    /**
     * Shift an Input off of the queue
     */
    public shift(): Input | undefined {
        return this.queue.shift();
    }
}

/**
 * A class to represent Ouput from the Engine. The Engine will queue this Output in a {@link Server}s
 * {@link Server.output_queue} to be sent through a {@link Connection}
 */
export class Output extends MUDObject {

    /**
     * The {@link Server.server_id} this Output is destined for
     */
    public server_id: string;

    /**
     * The corresponding {@link Connection.connection_id} this Output is destined for
     */
    public connection_id: string;

    /**
     * The message to buffer to send to the {@link Connection}
     */
    public buffer: string;

    /**
     * The type of the output this Output is. Can be one of the {@link IOType}s
     * @default IOType.OUTPUT
     */
    public type: IOType;

    constructor(server_id: string, connection_id: string, buffer: string, type: IOType = IOType.OUTPUT) {
        super();
        this.server_id = server_id;
        this.connection_id = connection_id;
        this.buffer = buffer;
        this.type = type;
    }
}

/**
 * An OutputQueue class to be registered with our Engine. This will act as a channel for the {@link Engine} to send it's
 * output through for the {@link Server} to send to {@link Connection}s. The {@link Engine} will push {@link Output} on
 * to the queue and the {@link Server} will send it.
 */
export class OutputQueue extends MUDQueue {

    /**
     * An internal array of {@link Output}s from the {@link Engine} to be buffered for the {@link Server} to process.
     * @protected
     */
    protected queue: Array<Output>;

    constructor() {
        super();
        this.queue = new Array<Output>();
    }

    /**
     * Push output on to the queue
     * @param output The output to send to the Server
     */
    public push(output: Output) {
        this.queue.push(output);
        return output;
    }

    /**
     * Shift an Output off of the queue
     */
    public shift(): Output | undefined {
        return this.queue.shift();
    }
}

/**
 * Our base Connection class that all Connection types should inherit from
 */
export abstract class Connection extends MUDObject {

    /**
     * The {@link Server.server_id} this Connection is associated with
     */
    public server_id: string = "";

    /**
     * A unique identifier for the Connection.
     */
    public abstract connection_id: string;

    /**
     * All Connections must provide a method for disconnecting a client. This will be called by the engine when the player
     * requests to close the connection through the game or the game determines the player needs to be disconnected.
     */
    public abstract disconnect(): void;

    /**
     * All Connections must provide a method for writing data to the connection
     */
    public abstract write(buff: string): void;
}

/**
 * The Base server class that all Servers should inherit from
 */
export abstract class Server extends MUDObject {

    /**
     * A unique identifier for the server. This is used by {@link Engine}s to know how to route output
     */
    public abstract server_id: string;

    /**
     * A dictionary of {@link Connection}s that should be subclassed for your specific server implementation
     */
    public abstract connections: {[connection_id: string]: Connection};

    /**
     * An input_queue for the Server to use. The {@link Connection} will fill the queue with input to be handled by the
     * {@link Engine}
     */
    public input_queue: InputQueue = new InputQueue();

    /**
     * An output_queue for the Server to use. The {@link Engine} will fill the queue with output to be sent
     */
    public output_queue: OutputQueue = new OutputQueue();

    /**
     * A method to remove a connection from the server.
     * @param connection_id A connection_id that will match {@link Connection.connection_id}
     */
    public remove_connection(connection_id: string): void {
        if(connection_id in this.connections)
            delete this.connections[connection_id];
        else
            console.warn(`UNABLE TO REMOVE CONNECTION ${connection_id} FROM SocketServer`);
    }

    /**
     * All servers must implement a start method which will start listening and handling incoming and outgoing communication
     */
    public abstract start(): void;
}
